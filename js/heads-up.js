// JavaScript for Heads-Up Notifications by Digital Dynasty Design

//Global Variable Declarations
var ani_toggle = 0;
var not_timeout;
var Notification = function(type, text, icon, link, interval, size) {
    this.type = type;
    this.text = text;
    this.icon = icon;
    this.link = link;
    this.interval = interval;
    this.size = size;
}
var not_array = [];
//End of Global Variable Declarations

function small_notification(type, text, icon, link, interval) {
    
    if (ani_toggle == 1)
    {
        var p_not = new Notification(type, text, icon, link, interval, 'small');
        not_array[not_array.length] = p_not;
        return false
    }
    ani_toggle = 1;
      
    var initial_pos;
    
    var basenotification = document.createElement('div');
    var notificationicon = document.createElement('div');
    var notificationiconi = document.createElement('i');
    var notificationtext = document.createElement('div');
    var nottexttype = document.createElement('div');
    var nottextcontent = document.createElement('div');
    var notificationclose = document.createElement('div');
    var notclosei = document.createElement('i');
    var alink = document.createElement('a');
    
    basenotification.setAttribute('class', 'not-base smallnb not-color-picker');
    basenotification.setAttribute('id', 'pop-down');
    basenotification.setAttribute('unselectable', 'on');
    notificationicon.setAttribute('class', 'not-icon smalli');
    notificationiconi.setAttribute('class', icon);
    notificationtext.setAttribute('class', 'not-text smallt');
    nottexttype.setAttribute('class', 'not-text-type');
    nottextcontent.setAttribute('class', 'not-text-content');
    notificationclose.setAttribute('class', 'not-close smallnc');
    notificationclose.setAttribute('id', 'not-close');
    notclosei.setAttribute('class', 'fa fa-times ');
    alink.setAttribute('href', link);
    alink.setAttribute('id', 'not-link');
    
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationicon).appendChild(notificationiconi);
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottexttype).appendChild(document.createTextNode(type));
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottextcontent).appendChild(document.createTextNode(text));
    document.body.appendChild(basenotification).appendChild(notificationclose).appendChild(notclosei);

    setTimeout(function(){document.getElementById('not-close').addEventListener('click', function() {
            close_notification();
        });}, 1000);
    
    

    touch_events();

    if (interval)
        {
        not_timeout = setTimeout(function(){close_notification()}, interval);
        }

   
}


function medium_notification(type, text, icon, link, interval) {
    
    if (ani_toggle == 1)
    {
        var p_not = new Notification(type, text, icon, link, interval, 'medium');
        not_array[not_array.length] = p_not;
        return false
    }
    ani_toggle = 1;
    
    var initial_pos;
    
    var basenotification = document.createElement('div');
    var notificationicon = document.createElement('div');
    var notificationiconi = document.createElement('i');
    var notificationtext = document.createElement('div');
    var nottexttype = document.createElement('div');
    var nottextcontent = document.createElement('div');
    var notificationclose = document.createElement('div');
    var notclosei = document.createElement('i');
    var alink = document.createElement('a');
    
    basenotification.setAttribute('class', 'not-base mediumnb not-color-picker');
    basenotification.setAttribute('id', 'pop-down');
    basenotification.setAttribute('unselectable', 'on');
    notificationicon.setAttribute('class', 'not-icon mediumi');
    notificationiconi.setAttribute('class', icon);
    notificationtext.setAttribute('class', 'not-text mediumt');
    nottexttype.setAttribute('class', 'not-text-type');
    nottextcontent.setAttribute('class', 'not-text-content');
    notificationclose.setAttribute('class', 'not-close mediumnc');
    notificationclose.setAttribute('id', 'not-close');
    notclosei.setAttribute('class', 'fa fa-times ');
    alink.setAttribute('href', link);
    alink.setAttribute('id', 'not-link');
    
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationicon).appendChild(notificationiconi);
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottexttype).appendChild(document.createTextNode(type));
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottextcontent).appendChild(document.createTextNode(text));
    document.body.appendChild(basenotification).appendChild(notificationclose).appendChild(notclosei);

    setTimeout(function(){document.getElementById('not-close').addEventListener('click', function() {
            close_notification();
        });}, 1000);

    
    touch_events();

    if (interval)
        {
        not_timeout = setTimeout(function(){close_notification()}, interval);
        }

   
}


function large_notification(type, text, icon, link, interval) {
    
    if (ani_toggle == 1)
    {
        var p_not = new Notification(type, text, icon, link, interval, 'large');
        not_array[not_array.length] = p_not;
        return false
    }
    ani_toggle = 1;
    
    var initial_pos;
    
    var basenotification = document.createElement('div');
    var notificationicon = document.createElement('div');
    var notificationiconi = document.createElement('i');
    var notificationtext = document.createElement('div');
    var nottexttype = document.createElement('div');
    var nottextcontent = document.createElement('div');
    var notificationclose = document.createElement('div');
    var notclosei = document.createElement('i');
    var alink = document.createElement('a');
    
    basenotification.setAttribute('class', 'not-base largenb not-color-picker');
    basenotification.setAttribute('id', 'pop-down');
    basenotification.setAttribute('unselectable', 'on');
    notificationicon.setAttribute('class', 'not-icon largei');
    notificationiconi.setAttribute('class', icon);
    notificationtext.setAttribute('class', 'not-text larget');
    nottexttype.setAttribute('class', 'not-text-type');
    nottextcontent.setAttribute('class', 'not-text-content');
    notificationclose.setAttribute('class', 'not-close largenc');
    notificationclose.setAttribute('id', 'not-close');
    notclosei.setAttribute('class', 'fa fa-times ');
    alink.setAttribute('href', link);
    alink.setAttribute('id', 'not-link');
    
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationicon).appendChild(notificationiconi);
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottexttype).appendChild(document.createTextNode(type));
    document.body.appendChild(basenotification).appendChild(alink).appendChild(notificationtext).appendChild(nottextcontent).appendChild(document.createTextNode(text));
    document.body.appendChild(basenotification).appendChild(notificationclose).appendChild(notclosei);

    setTimeout(function(){document.getElementById('not-close').addEventListener('click', function() {
            close_notification();
        });}, 1000);
    
    
    
    touch_events();
    

    if (interval)
        {
        not_timeout = setTimeout(function(){close_notification()}, interval);
        }

   
}




function close_notification()
{
    clearTimeout(not_timeout);
    document.getElementById('not-close').setAttribute('id', 'invalid');
    document.getElementById('pop-down').style.animationName = "close";
    setTimeout(function(){document.getElementById('pop-down').remove()}, 1000);
    ani_toggle = 0;
    check_not_array();
    
}

function touch_events()
{
    hoverTouchUnstick();
    var obj = document.getElementById('pop-down');
    obj.addEventListener('touchstart', function(event) {
    initial_pos = obj.offsetLeft;
    }, false);

    obj.addEventListener('touchmove', function(event) {
      // If there's exactly one finger inside this element
      if (event.targetTouches.length == 1) {
        var touch = event.targetTouches[0];
        var initial_pos = obj.style.left;
        // Place element where the finger is
        obj.style.left = touch.pageX + 'px';


      }
    }, false);

    obj.addEventListener('touchend', function(event) {
        var a = parseInt(obj.offsetLeft);
        var b = parseInt(initial_pos);
        var diff_pos = b-a;
        if ((diff_pos > -50) && (diff_pos < 50))
        {
            obj.style.left = "";
            
        }
        else 
        {
            clearTimeout(not_timeout);
            document.getElementById('pop-down').remove();
            ani_toggle = 0;
            check_not_array();
        }
    }, false);
}


function check_not_array()
{
   if (not_array.length == 0)
   {
       return false;
   }
    else if (not_array.length > 0)
    {
        window[not_array[0].size + '_notification'](not_array[0].type, not_array[0].text, not_array[0].icon, not_array[0].link, not_array[0].interval);
        not_array.splice(0, 1);
    }
}



Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function hoverTouchUnstick() {
  // Check if the device supports touch events
  if('ontouchstart' in document.documentElement) {
    // Loop through each stylesheet
    for(var sheetI = document.styleSheets.length - 1; sheetI >= 0; sheetI--) {
      var sheet = document.styleSheets[sheetI];
      // Verify if cssRules exists in sheet
      if(sheet.cssRules) {
        // Loop through each rule in sheet
        for(var ruleI = sheet.cssRules.length - 1; ruleI >= 0; ruleI--) {
          var rule = sheet.cssRules[ruleI];
          // Verify rule has selector text
          if(rule.selectorText) {
            // Replace hover psuedo-class with active psuedo-class
            rule.selectorText = rule.selectorText.replace(":hover", ":active");
          }
        }
      }
    }
  }
}

