document.addEventListener("DOMContentLoaded", function() {
        large_notification('Welcome', 'Try out the buttons below!', 'fa fa-thumbs-up', '#', 5000);
});

document.getElementById('example-button-1').addEventListener('click', function() {
        small_notification('Small', 'sample text', 'fa fa-comment', '#');
    });

document.getElementById('example-button-2').addEventListener('click', function() {
        medium_notification('Medium', 'I can fit more sample text', 'fa fa-thumbs-up', '#');
    });

document.getElementById('example-button-3').addEventListener('click', function() {
        large_notification('Large', 'I can fit even more sample text', 'fa fa-tags', '#');
    });

document.getElementById('example-button-4').addEventListener('click', function() {
        large_notification('Timed-Close', 'This will close in 4 Seconds', 'fa fa-clock-o', '#', 4000);
    });

